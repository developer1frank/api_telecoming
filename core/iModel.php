<?php
/**
 * Created by PhpStorm.
 * User: frankbriceno
 * Date: 15/6/17
 * Time: 0:05
 */

interface iModel
{
    // GET : Solicitar un elemento
    public function get();
    // POST : Publicar un nuevo elemento
    public function post();
    // PUT: Modificar un elemento
    public function put();
    // DELETE: Eliminar un elemento
    public function delete();
}